package com.example.mobileproject;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    private EditText login, password;
    private Button signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
        signInButton = (Button) findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            // Check user's logs
            @Override
            public void onClick(View v) {
                //read data from file
                String[] logs = new String[6];
                Scanner read = new Scanner(getResources().openRawResource(R.raw.logs));
                boolean access = false;
                while(read.hasNext()) {
                    logs = read.nextLine().split(" ", 6);
                    if(logs[1].equals(login.getText().toString()) && logs[3].equals(password.getText().toString())) {
                        access = true;
                        break;
                    } else    access = false;
                }
                // Notification about success
                if(access){
                    Toast success = Toast.makeText(getApplicationContext(),
                            "Авторизация прошла успешно!", Toast.LENGTH_SHORT);
                    success.show();
                    Intent webActivity = new Intent(MainActivity.this, WebViewActivity.class);
                    webActivity.putExtra("login", logs[1]);
                    webActivity.putExtra("website", logs[5]);
                    startActivity(webActivity);
                }
                // Notification about unsuccess
                else{
                    Toast failure = Toast.makeText(getApplicationContext(),
                            "Введены неверные данные!", Toast.LENGTH_SHORT);
                    failure.show();
                    login.setText("");
                    password.setText("");
                }
            }
        });
    }

}
