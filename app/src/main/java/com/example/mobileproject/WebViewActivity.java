package com.example.mobileproject;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class WebViewActivity extends AppCompatActivity {
    WebView webWindow;
    Toolbar toolbar;

    // create new changes in other branch!
    // create new changes in other branch!
    // create new changes in other branch!
    //create WebView object
    @Override
    protected void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // get data from other activity
        Intent intent = getIntent();
        String name = " ", website = " ";
        name = intent.getStringExtra("login");
        website = intent.getStringExtra("website");

        String fullTitle = name + " <- ./SearchEngines/.";
        getSupportActionBar().setTitle(fullTitle);

        webWindow = (WebView) findViewById(R.id.webWindow);
        webWindow.setWebViewClient(new WebViewClient());
        webWindow.loadUrl(website);
    }
    // func onBackPressed for WebView
    @Override
    public void onBackPressed() {
        if(webWindow.canGoBack()){
            webWindow.goBack();
        } else super.onBackPressed();
    }
}
